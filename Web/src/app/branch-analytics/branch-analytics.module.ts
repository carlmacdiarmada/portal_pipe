import { NgModule } from '@angular/core';
import { CommonModule as NgCommonModule } from '@angular/common';
import { UiSwitchModule } from 'ng2-ui-switch';
import { FormsModule } from '@angular/forms';
import { CommonModule } from './../common/common.module';
import { NgSpinKitModule } from 'ng-spin-kit'
import { NgxChartsModule } from '@swimlane/ngx-charts';

import { BranchAnalyticsComponent } from './branch-analytics/branch-analytics.component';
import { BranchAnalyticsGroupComponent } from './branch-analytics-group/branch-analytics-group.component';
import { BranchAnalyticsTimelineComponent } from './branch-analytics-timeline/branch-analytics-timeline.component';

@NgModule({
  declarations: [BranchAnalyticsComponent, BranchAnalyticsGroupComponent, BranchAnalyticsTimelineComponent],
  imports: [
    NgCommonModule,
    UiSwitchModule,
    FormsModule,
    CommonModule,
    NgSpinKitModule,
    NgxChartsModule,
  ]
})
export class BranchAnalyticsModule { }
