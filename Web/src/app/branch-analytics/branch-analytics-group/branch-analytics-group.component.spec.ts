import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BranchAnalyticsGroupComponent } from './branch-analytics-group.component';

describe('BranchAnalyticsGroupComponent', () => {
  let component: BranchAnalyticsGroupComponent;
  let fixture: ComponentFixture<BranchAnalyticsGroupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BranchAnalyticsGroupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BranchAnalyticsGroupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
