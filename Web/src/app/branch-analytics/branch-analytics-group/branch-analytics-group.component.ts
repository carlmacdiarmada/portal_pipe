import { Component, OnChanges, SimpleChanges, Input } from '@angular/core';
import { WIDGET_STATE } from './../../models/Enums';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { JobExecutionResult } from './../../models/JobExecutionResult';
import { chain as _chain } from 'lodash';

@Component({
  selector: 'branch-analytics-group',
  templateUrl: './branch-analytics-group.component.html',
  styleUrls: ['./branch-analytics-group.component.less']
})
export class BranchAnalyticsGroupComponent implements OnChanges {

  @Input() results: JobExecutionResult[];
  @Input() state: WIDGET_STATE;

  groupedResults: any[];
  WIDGET_STATE: any = WIDGET_STATE;

  colorScheme = {
    domain: ['#f37f4f']
  };

  constructor(private modalService: NgbModal) { }

  ngOnChanges(changesObj: SimpleChanges) {
    if (this.results) {
      this.groupData();
    }
  }

  showHelp(modal: any) {
    this.modalService.open(modal);
  }

  groupData(): void {
    this.groupedResults = [];

    for (let i = 0; i <= 110; i += 5) {
      const group = {
        name: `${i + 5}`,
        value: this.results.filter(item => this.resultFilter(item, i, i + 5)).length
      };

      this.groupedResults.push(group);
    }

    const lastGroup = {
      name: `120+`,
      value: this.results.filter(item => this.resultFilter(item, 120, 999)).length
    };

    this.groupedResults.push(lastGroup);

  }

  resultFilter(item: JobExecutionResult, start: number, end: number): boolean {
    const timeTaken = item.waitTime - item.blockedTime + item.duration;

    return timeTaken >= start && timeTaken <= end;
  }

  tooltipFormatter(group: any): string {
    return `${group.value} jobs ran between ${group.name - 5} - ${group.name} mins`;
  }
}
