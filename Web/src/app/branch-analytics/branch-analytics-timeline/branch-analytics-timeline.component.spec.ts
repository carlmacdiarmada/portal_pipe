import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BranchAnalyticsTimelineComponent } from './branch-analytics-timeline.component';

describe('BranchAnalyticsTimelineComponent', () => {
  let component: BranchAnalyticsTimelineComponent;
  let fixture: ComponentFixture<BranchAnalyticsTimelineComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BranchAnalyticsTimelineComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BranchAnalyticsTimelineComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
