import { Component, OnChanges, SimpleChanges, Input } from '@angular/core';
import { WIDGET_STATE } from './../../models/Enums';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { JobExecutionResult } from './../../models/JobExecutionResult';
import { groupBy as _groupBy, map as _map, sortBy as _sortBy, chain as _chain } from 'lodash';

@Component({
  selector: 'branch-analytics-timeline',
  templateUrl: './branch-analytics-timeline.component.html',
  styleUrls: ['./branch-analytics-timeline.component.less']
})
export class BranchAnalyticsTimelineComponent implements OnChanges {

  @Input() results: JobExecutionResult[];
  @Input() state: WIDGET_STATE;

  groupedResults: any;
  WIDGET_STATE: any = WIDGET_STATE;

  colorScheme = {
    domain: ['#f37f4f', '#313335']
  };

  constructor(private modalService: NgbModal) { }

  ngOnChanges(changesObj: SimpleChanges) {
    if (this.results) {
      this.groupData();
    }
  }

  showHelp(modal: any) {
    this.modalService.open(modal);
  }

  groupData(): void {
    const x = this.results.map(item => {
      return {
        name: item.startedAt,
        value: item.waitTime - item.blockedTime + item.duration
      };
    });

    const y = this.results.map(item => {
      return {
        name: item.startedAt,
        value: item.blockedTime
      };
    });

    this.groupedResults = [
      {
        name: 'Time Taken',
        series: x
      },
      {
        name: 'Blocked',
        series: y
      }
    ];
  }

  tickFormatter(value: string) {
    const d = new Date(value);
    return d.getDate() + '/' + (d.getMonth() + 1);
  }
}
