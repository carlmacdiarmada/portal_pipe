import { Component, OnInit } from '@angular/core';
import { DataService } from './../../services/data.service';
import { finalize } from 'rxjs/operators';
import { JobExecutionResult } from './../../models/JobExecutionResult';
import { BranchAnalyticsCriteria } from './../../models/BranchAnalyticsCriteria';
import { WIDGET_STATE, DATE_RANGE_TYPE, BUILD_TYPE, JOB_OUTCOME_TYPE } from './../../models/Enums';
import { mean as _mean, sortBy as _sortBy } from 'lodash';

@Component({
  selector: 'branch-analytics',
  templateUrl: './branch-analytics.component.html',
  styleUrls: ['./branch-analytics.component.less']
})
export class BranchAnalyticsComponent implements OnInit {

  WIDGET_STATE: any = WIDGET_STATE;
  DATE_RANGE_TYPE: any = DATE_RANGE_TYPE;
  BUILD_TYPE: any = BUILD_TYPE;
  JOB_OUTCOME_TYPE: any = JOB_OUTCOME_TYPE;
  state: WIDGET_STATE = WIDGET_STATE.Unitialized;

  criteria: BranchAnalyticsCriteria;
  results: JobExecutionResult[];
  showMedian = false;

  avgWait: number = null;
  avgBlock: number = null;
  avgDuration: number = null;
  avgTimeTaken: number = null;

  constructor(private dataService: DataService) { }

  ngOnInit() {
    this.criteria = ({
      dateRange: this.DATE_RANGE_TYPE.Last30Days,
      buildType: this.BUILD_TYPE.All,
      outcomeType: this.JOB_OUTCOME_TYPE.All,
      includeWeekends: false
    }) as BranchAnalyticsCriteria;

    this.getBranchAnalytics();
  }

  getBranchAnalytics() {
    this.state = WIDGET_STATE.Loading;

    const source = this.dataService.getBranchAnalytics(this.criteria);

    source.pipe(
      finalize(() => {}),
    ).subscribe(this.httpSuccess.bind(this), this.httpError);
  }

  httpSuccess(results: JobExecutionResult[]) {
    this.state = WIDGET_STATE.Success;

    this.results = results;

    this.aggregateData();
  }

  httpError(ex) {
    this.state = WIDGET_STATE.Failure;

    if (ex.status === 500) {
      // Internal Server Error
      // this.errMsg = ex.error.exceptionMessage;
      // this.errStackTrace = ex.error.stackTrace;
    }
  }

  updateSwtich(propNo: number) {
    if (propNo === 1) {
      this.criteria.includeWeekends = !this.criteria.includeWeekends;
    }

    if (propNo === 2) {
      this.showMedian = !this.showMedian;
    }

    this.getBranchAnalytics();
  }

  aggregateData() {
    if (!this.showMedian) {
      this.aggregateDataMean();
    } else {
      this.aggregateDataMedian();
    }
  }

  aggregateDataMean() {
    this.avgWait = +(_mean(this.results.map(r => r.waitTime))).toFixed(1) || 0;
    this.avgBlock = +(_mean(this.results.map(r => r.blockedTime))).toFixed(1)  || 0;
    this.avgDuration = +(_mean(this.results.map(r => r.duration))).toFixed(1) || 0;
    this.avgTimeTaken = +(_mean(this.results.map(r => (r.waitTime - r.blockedTime + r.duration)))).toFixed(1) || 0;
  }

  aggregateDataMedian() {
    const ind = Math.round(this.results.length / 2);

    const sortedByWait = _sortBy(this.results.map(r => r.waitTime));
    const sortedByBlock = _sortBy(this.results.map(r => r.blockedTime));
    const sortedByDuration = _sortBy(this.results.map(r => r.duration));

    this.avgWait = +(sortedByWait[ind]).toFixed(1) || 0;
    this.avgBlock = +(sortedByBlock[ind]).toFixed(1)  || 0;
    this.avgDuration = +(sortedByDuration[ind]).toFixed(1) || 0;
    this.avgTimeTaken = +(this.avgWait - this.avgBlock + this.avgDuration).toFixed(1) || 0;
  }

}
