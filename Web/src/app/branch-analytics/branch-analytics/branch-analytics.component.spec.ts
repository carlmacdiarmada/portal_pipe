import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BranchAnalyticsComponent } from './branch-analytics.component';

describe('BranchAnalyticsComponent', () => {
  let component: BranchAnalyticsComponent;
  let fixture: ComponentFixture<BranchAnalyticsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BranchAnalyticsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BranchAnalyticsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
