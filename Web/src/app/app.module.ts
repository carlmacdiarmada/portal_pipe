import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule }    from '@angular/common/http';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { CommonModule } from './common/common.module';
import { DrilldownModule } from './drilldown/drilldown.module';
import { BranchComparisonModule } from './branch-comparison/branch-comparison.module';
import { BranchLeaderboardModule } from './branch-leaderboard/branch-leaderboard.module';
import { BranchAnalyticsModule } from './branch-analytics/branch-analytics.module';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    NgbModule,
    HttpClientModule,
    CommonModule,
    DrilldownModule,
    BranchComparisonModule,
    BranchLeaderboardModule,
    BranchAnalyticsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
