import { TestResult } from './models/TestResult';
import { orderBy as _orderBy } from 'lodash';
import { SortableHeader } from './models/SortableHeader';

export function getTestResultObject(totalPassed: number, totalFailed: number, totalSkipped: number, linesCovered?: number, totalLines?: number): TestResult {
    const total = totalPassed + totalFailed + totalSkipped;

    const passed = precisionRound((totalPassed / total) * 100, 2);
    const failed = precisionRound((totalFailed / total) * 100, 2);
    const skipped = precisionRound((totalSkipped / total) * 100, 2);
    let coverage = null;

    if (linesCovered && totalLines) {
      coverage = precisionRound((linesCovered / totalLines) * 100, 2);
    }

    return <TestResult>({
      totalPassed: totalPassed,
      totalFailed: totalFailed,
      totalSkipped: totalSkipped,
      total: total,
      passed: passed,
      failed: failed,
      skipped: skipped,
      coverage: coverage
    });
  }

  function precisionRound(number: number, precision: number) {
    var factor = Math.pow(10, precision);
    return Math.round(number * factor) / factor;
  }

  export function sortData<T>(header: SortableHeader, headerCollection: SortableHeader[], data: T[]){
    if (header.direction === null) {
      headerCollection.map(h => {if (h.allowSort) {h.direction = null}});
      header.direction = 'asc';
    }
    else if (header.direction === 'asc') {
      header.direction = 'desc';
    }
    else if (header.direction === 'desc') {
      header.direction = 'asc';
    }

    return _orderBy(data, header.propName, header.direction);
  }