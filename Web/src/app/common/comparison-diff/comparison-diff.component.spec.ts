import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ComparisonDiffComponent } from './comparison-diff.component';

describe('ComparisonDiffComponent', () => {
  let component: ComparisonDiffComponent;
  let fixture: ComponentFixture<ComparisonDiffComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ComparisonDiffComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ComparisonDiffComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
