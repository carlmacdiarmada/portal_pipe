import { Component, OnChanges, Input } from '@angular/core';

@Component({
  selector: 'comparison-diff',
  templateUrl: './comparison-diff.component.html',
  styleUrls: ['./comparison-diff.component.less']
})
export class ComparisonDiffComponent implements OnChanges {

  @Input() source: number;
  @Input() target: number;
  @Input() srccoverage: number;
  @Input() trgcoverage: number;
  result: string = null;
  response: string = null;
  isPositive: boolean = false;

  constructor() { }

  ngOnChanges() {
    this.result = this.getResult();
  }

  getResult(): string {
    this.source = this.source || 0;
    this.target = this.target || 0;
    this.srccoverage = this.srccoverage || 0;
    this.trgcoverage = this.trgcoverage || 0;


    if ((this.source - this.target) > 0) {
      this.isPositive = true;
      this.response = '+' + (this.source - this.target) + " Tests";
    }
    else if ((this.source - this.target) === 0) {
      this.response = '';
    }
    else {
      this.response = (this.source - this.target).toString() + " Tests";
    }

    if ((this.srccoverage - this.trgcoverage) > 0) {
      this.isPositive = true;
      this.response += '+' + (Math.fround(this.srccoverage - this.trgcoverage)).toFixed(2) + "%";
    }
    else if ((this.srccoverage - this.trgcoverage) === 0) {
      this.response += '';
    }
    else {
      this.response += (Math.fround(this.srccoverage - this.trgcoverage)).toFixed(2).toString() + "%";
    }

      return this.response;
  }
}
