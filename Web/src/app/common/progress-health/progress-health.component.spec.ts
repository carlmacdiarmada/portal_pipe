import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProgressHealthComponent } from './progress-health.component';

describe('ProgressHealthComponent', () => {
  let component: ProgressHealthComponent;
  let fixture: ComponentFixture<ProgressHealthComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProgressHealthComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProgressHealthComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
