import { NgModule } from "@angular/core";
import { CommonModule as NgCommonModule } from "@angular/common";
import { RouterModule } from "@angular/router";
import { NgSpinKitModule } from "ng-spin-kit";

import { TopNavComponent } from "./top-nav/top-nav.component";
import { ProgressHealthComponent } from "./progress-health/progress-health.component";
import { ProgressHealthReport } from "./health-report/health-report.component";
import { StatusIndicatorComponent } from "./status-indicator/status-indicator.component";
import { ComparisonDiffComponent } from "./comparison-diff/comparison-diff.component";
import { ComparisonCardComponent } from "./comparison-card/comparison-card.component";
import { BranchDistanceComponent } from "./branch-distance/branch-distance.component";
import { NumberCardComponent } from "./number-card/number-card.component";

@NgModule({
  declarations: [
    TopNavComponent,
    ProgressHealthComponent,
    ProgressHealthReport,
    StatusIndicatorComponent,
    ComparisonDiffComponent,
    ComparisonCardComponent,
    BranchDistanceComponent,
    NumberCardComponent
  ],
  imports: [NgCommonModule, RouterModule, NgSpinKitModule],
  exports: [
    TopNavComponent,
    ProgressHealthComponent,
    ProgressHealthReport,
    StatusIndicatorComponent,
    ComparisonDiffComponent,
    ComparisonCardComponent,
    BranchDistanceComponent,
    NumberCardComponent
  ]
})
export class CommonModule {}
