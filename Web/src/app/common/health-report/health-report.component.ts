import { Component, OnChanges, Input } from "@angular/core";
import { TestResult } from "../../models/TestResult";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";

@Component({
  selector: "health-report",
  templateUrl: "./health-report.component.html",
  styleUrls: ["./health-report.component.less"]
})
export class ProgressHealthReport implements OnChanges {
  @Input() testResults: TestResult;
  @Input() showNoCoverage: boolean;
  @Input() flashOnFailed: boolean = false;

  coverageClass: string;
  showProgressHealth: boolean;

  constructor(private modalService: NgbModal) {}

  ngOnChanges() {
    this.coverageClass = this.getCoverageClass();
    this.showProgressHealth =
      this.testResults &&
      this.testResults.passed +
        this.testResults.failed +
        this.testResults.skipped >
        0;
  }

  showDetails(modal: any) {
    this.modalService.open(modal);
  }

  getCoverageClass(): string {
    if (!this.testResults || !this.testResults.coverage) {
      return this.showNoCoverage ? "no-coverage" : "coverage-90";
    }

    const coverage = this.testResults.coverage;

    if (coverage <= 20) {
      return "coverage-20";
    } else if (coverage >= 20 && coverage <= 30) {
      return "coverage-30";
    } else if (coverage >= 30 && coverage <= 40) {
      return "coverage-40";
    } else if (coverage >= 40 && coverage <= 50) {
      return "coverage-50";
    } else if (coverage >= 50 && coverage <= 60) {
      return "coverage-60";
    } else if (coverage >= 60 && coverage <= 70) {
      return "coverage-70";
    } else if (coverage >= 70 && coverage <= 80) {
      return "coverage-80";
    } else if (coverage >= 80) {
      return "coverage-90";
    }
  }
}
