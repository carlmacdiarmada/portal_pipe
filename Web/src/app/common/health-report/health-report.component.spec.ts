import { async, ComponentFixture, TestBed } from "@angular/core/testing";

import { ProgressHealthReport } from "./health-report.component";

describe("ProgressHealthReport", () => {
  let component: ProgressHealthReport;
  let fixture: ComponentFixture<ProgressHealthReport>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ProgressHealthReport]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProgressHealthReport);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
