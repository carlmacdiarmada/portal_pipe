import { Component, OnChanges, SimpleChanges, Input } from '@angular/core';
import { WIDGET_STATE } from './../../models/Enums';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'number-card',
  templateUrl: './number-card.component.html',
  styleUrls: ['./number-card.component.less']
})
export class NumberCardComponent implements OnChanges {

  @Input() title: string;
  @Input() number: number;
  @Input() state: WIDGET_STATE;

  WIDGET_STATE: any = WIDGET_STATE;

  constructor(private modalService: NgbModal) { }

  ngOnChanges(changesObj: SimpleChanges) {
  }

  showHelp(modal: any) {
    this.modalService.open(modal);
  }

}
