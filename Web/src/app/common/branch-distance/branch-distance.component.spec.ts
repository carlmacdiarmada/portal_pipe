import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BranchDistanceComponent } from './branch-distance.component';

describe('BranchDistanceComponent', () => {
  let component: BranchDistanceComponent;
  let fixture: ComponentFixture<BranchDistanceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BranchDistanceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BranchDistanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
