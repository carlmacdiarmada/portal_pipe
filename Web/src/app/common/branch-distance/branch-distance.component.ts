import { Component, OnChanges, SimpleChanges, Input } from '@angular/core';
import { WIDGET_STATE } from './../../models/Enums';
import { DataService } from './../../services/data.service';
import { finalize } from 'rxjs/operators';
import { BranchStats } from './../../models/BranchStats';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'branch-distance',
  templateUrl: './branch-distance.component.html',
  styleUrls: ['./branch-distance.component.less']
})
export class BranchDistanceComponent implements OnChanges {

  @Input() sourceBranch: string;

  state: WIDGET_STATE = WIDGET_STATE.Unitialized;
  WIDGET_STATE: any = WIDGET_STATE;
  stats: BranchStats = null;

  errMsg: string = null;
  errStackTrace: string = null;

  constructor(private dataService: DataService, private modalService: NgbModal) { }

  ngOnChanges(changesObj: SimpleChanges) {
    if (this.sourceBranch) {
      this.getBranchStats();
    }
  }

  getBranchStats() {
    this.state = WIDGET_STATE.Loading;

    let source = this.dataService.getBranchStats(this.sourceBranch);

    source.pipe(
      finalize(() => {}),
    ).subscribe(this.httpSuccess.bind(this), this.httpError);
  }

  httpSuccess(stats: BranchStats){
    this.state = WIDGET_STATE.Success;
    this.stats = stats;
  }

  httpError(ex) {
    this.state = WIDGET_STATE.Failure;

    if (ex.status === 500) {
      //Internal Server Error
      this.errMsg = ex.error.exceptionMessage;
      this.errStackTrace = ex.error.stackTrace;
    }
  }

  showHelp(modal: any) {
    this.modalService.open(modal);
  }

}
