import { Component, OnInit, Input } from '@angular/core';
import { JobStatus } from './../../models/JobStatus';

@Component({
  selector: 'status-indicator',
  templateUrl: './status-indicator.component.html',
  styleUrls: ['./status-indicator.component.less']
})
export class StatusIndicatorComponent implements OnInit {

  @Input() jobStatus: JobStatus;
  class: string;
  value: string;
  showText: boolean;


  constructor() { }

  ngOnInit() {
    this.showText = false;
    this.class = this.getClass();
    this.value = this.getText();
  }

  getClass(): string {
    switch (this.jobStatus.outcome) {
      case "S":
        return "success";
      case "F":
        return "danger";
      case "U":
        return "warning";
      case "A":
        return "primary";
    }
  }

  getText(): string {
    switch (this.jobStatus.outcome) {
      case "S":
        return "Passing";
      case "F":
        return "Failing";
      case "U":
        return "Unstable";
      case "A":
        return "Abandoned";
    }
  }

}
