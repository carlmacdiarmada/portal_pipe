import { Component, OnChanges, SimpleChanges, Input } from '@angular/core';
import { WIDGET_STATE } from './../../models/Enums';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'comparison-card',
  templateUrl: './comparison-card.component.html',
  styleUrls: ['./comparison-card.component.less']
})
export class ComparisonCardComponent implements OnChanges {

  @Input() title: string;
  @Input() source: number;
  @Input() target: number;
  @Input() state: WIDGET_STATE;

  WIDGET_STATE: any = WIDGET_STATE;
  result: number = null;

  constructor(private modalService: NgbModal) { }

  ngOnChanges(changesObj: SimpleChanges) {
    if (this.state && this.state === this.WIDGET_STATE.Success) {
      if (this.source && this.target){
        this.result = this.source - this.target;
      }
      else {
        this.result = null;
      }
    }
  }

  showHelp(modal: any) {
    this.modalService.open(modal);
  }
}
