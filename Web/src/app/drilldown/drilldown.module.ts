import { NgModule } from '@angular/core';
import { CommonModule as NgCommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { NgSpinKitModule } from 'ng-spin-kit'
import { CommonModule } from './../common/common.module';
import { FormsModule } from '@angular/forms';

import { DrilldownComponent } from './drilldown/drilldown.component';
import { ReposHealthComponent } from './repos-health/repos-health.component';
import { BranchViewComponent } from './branch-view/branch-view.component';
import { BranchHealthComponent } from './branch-health/branch-health.component';

@NgModule({
  declarations: [
    DrilldownComponent,
    ReposHealthComponent,
    BranchViewComponent,
    BranchHealthComponent
  ],
  imports: [
    RouterModule,
    NgSpinKitModule,
    NgCommonModule,
    CommonModule,
    FormsModule
  ],
  exports: [
    DrilldownComponent,
    BranchViewComponent
  ]
})
export class DrilldownModule { }
