import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReposHealthComponent } from './repos-health.component';

describe('ReposHealthComponent', () => {
  let component: ReposHealthComponent;
  let fixture: ComponentFixture<ReposHealthComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReposHealthComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReposHealthComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
