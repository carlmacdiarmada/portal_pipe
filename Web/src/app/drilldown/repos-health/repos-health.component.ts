import { Component, OnInit } from '@angular/core';
import { DataService } from './../../services/data.service';
import { RepoHealth } from './../../models/RepoHealth';
import { WIDGET_STATE } from './../../models/Enums';
import { finalize } from 'rxjs/operators';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { SortableHeader } from '../../models/SortableHeader';
import { sortData } from './../../utils';

@Component({
  selector: 'repos-health',
  templateUrl: './repos-health.component.html',
  styleUrls: ['./repos-health.component.less']
})
export class ReposHealthComponent implements OnInit {

  repos: RepoHealth[];
  WIDGET_STATE = WIDGET_STATE;
  state: WIDGET_STATE = WIDGET_STATE.Unitialized;
  errMsg: string = "";
  errStackTrace: string = "";
  headerCollection: SortableHeader[];

  constructor(private dataService: DataService, private modalService: NgbModal) { }

  ngOnInit() {
    this.getRepos();

    this.headerCollection = [
      <SortableHeader>({allowSort: true, displayName: 'Branch Name', propName: 'branch', direction: null}),
      <SortableHeader>({allowSort: true, displayName: 'Passed', propName: 'testResults.totalPassed', direction: null}),
      <SortableHeader>({allowSort: true, displayName: 'Failed', propName: 'testResults.totalFailed', direction: null}),
      <SortableHeader>({allowSort: true, displayName: 'Skipped', propName: 'testResults.totalSkipped', direction: null}),
      <SortableHeader>({allowSort: false, displayName: 'Health'}),
    ]
  }

  getRepos(): void {
    let source = this.dataService.getReposHealth();

    source.pipe(
      finalize(() => {}),
    ).subscribe(
      (repos: RepoHealth[]) => {
        this.repos = repos;
        this.state = WIDGET_STATE.Success;
      },
      ex => {
        this.state = WIDGET_STATE.Failure;

        if (ex.status === 500) {
          //Internal Server Error
          this.errMsg = ex.error.exceptionMessage;
          this.errStackTrace = ex.error.stackTrace;
        }
      }
    );
  }

  showHelp(modal: any): void {
    this.modalService.open(modal);
  }

  sort(header: SortableHeader): void {
    this.repos = sortData(header, this.headerCollection, this.repos);
  }
}
