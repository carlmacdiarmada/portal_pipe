import { Component, OnInit } from '@angular/core';
import { DataService } from './../../services/data.service';
import { Branch } from './../../models/Branch';
import { JobStatus } from './../../models/JobStatus';
import { finalize } from 'rxjs/operators';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'branch-view',
  templateUrl: './branch-view.component.html',
  styleUrls: ['./branch-view.component.less']
})
export class BranchViewComponent implements OnInit {

  branches: Branch[];
  jobStatuses: JobStatus[];
  selectedBranch: Branch;
  branchId: any

  constructor(private dataService: DataService, private route: ActivatedRoute) { }

  ngOnInit() {
    this.branchId = +this.route.snapshot.paramMap.get('branchId');

    this.getBranches();
  }

  getBranches(): void {
    let source = this.dataService.getBranches();

    source.pipe(
      finalize(() => {}),
    ).subscribe(
      (branches: Branch[]) => {
        this.branches = branches;
        this.selectedBranch = this.branches.find(o => o.branchId === this.branchId);
        this.getBranchStatus();
      },
      ex => {
        if (ex.status === 500) {
          //Internal Server Error
          console.error(ex.error.exceptionMessage, ex.error.stackTrace);
        }
      }
    );
  }

  getBranchStatus(): void {
    let source = this.dataService.getBranchStatus(this.selectedBranch.branchId);

    source.pipe(
      finalize(() => {}),
    ).subscribe(
      (jobStatuses: JobStatus[]) => {
        this.jobStatuses = jobStatuses;
      },
      ex => {
        if (ex.status === 500) {
          //Internal Server Error
          console.error(ex.error.exceptionMessage, ex.error.stackTrace);
        }
      }
    );
  }

}
