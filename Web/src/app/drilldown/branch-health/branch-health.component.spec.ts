import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BranchHealthComponent } from './branch-health.component';

describe('BranchHealthComponent', () => {
  let component: BranchHealthComponent;
  let fixture: ComponentFixture<BranchHealthComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BranchHealthComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BranchHealthComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
