import { Component, Input, OnChanges } from '@angular/core';
import { DataService } from './../../services/data.service';
import { JobResult } from './../../models/JobResult';
import { WIDGET_STATE } from './../../models/Enums';
import { finalize } from 'rxjs/operators';
import { Branch } from './../../models/Branch';
import { CommitTime } from './../../models/CommitTime';
import { Commit } from './../../models/Commit';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { SortableHeader } from '../../models/SortableHeader';
import { sortData } from './../../utils';

@Component({
  selector: 'branch-health',
  templateUrl: './branch-health.component.html',
  styleUrls: ['./branch-health.component.less']
})
export class BranchHealthComponent implements OnChanges {

  @Input() branch: Branch;

  jobResults: JobResult[];
  filteredResults: JobResult[];
  WIDGET_STATE = WIDGET_STATE;
  state: WIDGET_STATE = WIDGET_STATE.Unitialized;
  errMsg: string = "";
  errStackTrace: string = "";
  showAll: boolean = false;
  commit: Commit;
  sha: string;
  MIN_DATE: Date = new Date(-8640000000000000);
  headerCollection: SortableHeader[];
  ciserver: string = "http://ciserver:8080";

  constructor(private dataService: DataService, private modalService: NgbModal) { }

  ngOnInit() {
    this.headerCollection = [
      <SortableHeader>({allowSort: true, displayName: 'Job Name', propName: 'jobName', direction: null}),
      <SortableHeader>({allowSort: false, displayName: 'Unit Tests'}),
      <SortableHeader>({allowSort: false, displayName: 'Integ Tests'}),
      <SortableHeader>({allowSort: false, displayName: 'XAT Tests'}),
      <SortableHeader>({allowSort: false, displayName: 'SHA Id'}),
      <SortableHeader>({allowSort: false, displayName: 'Last SHA Id'}),
    ]
  }

  ngOnChanges() {
    this.getBranchHealth();
  }

  getBranchHealth(): void {
    let source = this.dataService.getBranchHealth(this.branch.branchId);

    source.pipe(
      finalize(() => {}),
    ).subscribe(
      (jobResults: JobResult[]) => {
        this.jobResults = jobResults;
        this.state = WIDGET_STATE.Success;

        this.updateFilter();

        //Load Commit Times Async
        this.getCommitTimes();
      },
      ex => {
        this.state = WIDGET_STATE.Failure;

        if (ex.status === 500) {
          //Internal Server Error
          this.errMsg = ex.error.exceptionMessage;
          this.errStackTrace = ex.error.stackTrace;
        }
      }
    );
  }

  filterResults() {
    this.filteredResults = this.jobResults.filter(
      jr =>
        (jr.unitTestResults && jr.unitTestResults.totalFailed > 0) ||
        (jr.integTestResults && jr.integTestResults.totalFailed > 0) ||
        (jr.xatTestResults && jr.xatTestResults.totalFailed > 0)
    );
  }

  updateFilter() {
    if (this.showAll) {
      this.filteredResults = this.jobResults;
    } else {
      this.filteredResults = this.jobResults.filter(
        jr =>
          (jr.unitTestResults && jr.unitTestResults.totalFailed > 0) ||
          (jr.integTestResults && jr.integTestResults.totalFailed > 0) ||
          (jr.xatTestResults && jr.xatTestResults.totalFailed > 0)
      );
    }

  }

  getCommitTimes(): void {
    this.jobResults.map(result => {
      let source = this.dataService.getCommitTimes(this.branch.repoName, result.shaId, result.lastShaId);

      source.pipe(
        finalize(() => {}),
      ).subscribe(
        (commitTime: CommitTime) => {
          result.commitTime = commitTime ? commitTime.item1 : this.MIN_DATE;
          result.lastCommitTime = commitTime ? commitTime.item2 : this.MIN_DATE;
        },
        ex => {
          result.commitTime = null;
        });
    });
  }

  viewCommitDetails(modal: any, shaId: string): void {
    let source = this.dataService.getCommitDetails(this.branch.repoName, shaId);

    source.pipe(
      finalize(() => {}),
    ).subscribe(
      (commit: Commit) => {
        this.sha = shaId;
        this.commit = commit;

        this.modalService.open(modal);
      },
      ex => {
        this.state = WIDGET_STATE.Failure;

        if (ex.status === 500) {
          //Internal Server Error
          this.errMsg = ex.error.exceptionMessage;
          this.errStackTrace = ex.error.stackTrace;
        }
      }
    );
  }

  getFailingJobs(jr: JobResult): string {
    let output: string = '';

    if (jr && jr.unitTestResults && jr.unitTestResults.totalFailed > 0) {
      output += `${jr.unitTestResults.totalFailed} UT,`;
    }

    if (jr && jr.integTestResults && jr.integTestResults.totalFailed > 0) {
      output += `${jr.integTestResults.totalFailed} IT,`;
    }

    if (jr && jr.xatTestResults && jr.xatTestResults.totalFailed > 0) {
      output += `${jr.xatTestResults.totalFailed} XAT,`;
    }

    if (output.length) {
      output = output.substr(0, output.length - 1);
    }

    return output;
  }

  showHelp(modal: any) {
    this.modalService.open(modal);
  }

  sort(header: SortableHeader): void {
    this.filteredResults = sortData(header, this.headerCollection, this.filteredResults);
  }
}
