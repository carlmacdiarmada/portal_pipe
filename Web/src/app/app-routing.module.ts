import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DrilldownComponent }  from './drilldown/drilldown/drilldown.component';
import { BranchComparisonComponent }  from './branch-comparison/branch-comparison/branch-comparison.component';
import { BranchViewComponent } from './drilldown/branch-view/branch-view.component';
import { BranchLeaderboardComponent } from './branch-leaderboard/branch-leaderboard/branch-leaderboard.component';
import { BranchAnalyticsComponent } from './branch-analytics/branch-analytics/branch-analytics.component';

const routes: Routes = [
  { path: '', redirectTo: '/drilldown', pathMatch: 'full' },
  { path: 'drilldown', component: DrilldownComponent },
  { path: 'branch/:branchId', component: BranchViewComponent },
  { path: 'branchComparison', component: BranchComparisonComponent },
  { path: 'branchLeadboard', component: BranchLeaderboardComponent },
  { path: 'branchAnalytics', component: BranchAnalyticsComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
