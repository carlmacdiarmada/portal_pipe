import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BranchLeaderboardComponent } from './branch-leaderboard.component';

describe('BranchLeaderboardComponent', () => {
  let component: BranchLeaderboardComponent;
  let fixture: ComponentFixture<BranchLeaderboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BranchLeaderboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BranchLeaderboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
