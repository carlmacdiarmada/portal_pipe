import { Component, OnInit } from '@angular/core';
import { DataService } from './../../services/data.service';
import { finalize } from 'rxjs/operators';
import { BranchDetails } from './../../models/BranchDetails';
import { WIDGET_STATE } from './../../models/Enums';
import { SortableHeader } from '../../models/SortableHeader';
import { sortData } from './../../utils';
import { find as _find, without as _without } from 'lodash';

@Component({
  selector: 'branch-leaderboard',
  templateUrl: './branch-leaderboard.component.html',
  styleUrls: ['./branch-leaderboard.component.less']
})
export class BranchLeaderboardComponent implements OnInit {

  branchLeaderboard: BranchDetails[];
  devBranch: BranchDetails = null;
  WIDGET_STATE: any = WIDGET_STATE;
  state: WIDGET_STATE = WIDGET_STATE.Unitialized;
  errMsg: string = "";
  errStackTrace: string = "";
  headerCollection: SortableHeader[];

  constructor(private dataService: DataService) { }

  ngOnInit() {
    this.headerCollection = [
      <SortableHeader>({allowSort: false, displayName: 'Branch Name', cssClass: 'sortable col-md-4'}),
      <SortableHeader>({allowSort: true, displayName: 'Score', propName: 'score', direction: 'asc', cssClass: 'sortable col-md-2 text-right'}),
      <SortableHeader>({allowSort: false, displayName: 'Behind', cssClass: 'sortable col-md-2 text-right'}),
      <SortableHeader>({allowSort: false, displayName: 'Ahead', cssClass: 'sortable col-md-1 text-right'}),
      <SortableHeader>({allowSort: false, displayName: 'Unit', cssClass: 'sortable col-md-1 text-right'}),
      <SortableHeader>({allowSort: false, displayName: 'Integ', cssClass: 'sortable col-md-1 text-right'}),
      <SortableHeader>({allowSort: false, displayName: 'XAT' , cssClass: 'sortable col-md-1 text-right'}),
    ]

    this.getBranchLeaderboard();
  }

  getBranchLeaderboard() {
    this.state = WIDGET_STATE.Loading;

    let source = this.dataService.getBranchLeaderboard();

    source.pipe(
      finalize(() => {}),
    ).subscribe(this.httpSuccess.bind(this), this.httpError);
  }

  httpSuccess(branchLeaderboard: BranchDetails[]){
    this.state = WIDGET_STATE.Success;

    this.branchLeaderboard = branchLeaderboard;

    this.compareToDev();
  }

  httpError(ex) {
    this.state = WIDGET_STATE.Failure;

    if (ex.status === 500) {
      //Internal Server Error
      this.errMsg = ex.error.exceptionMessage;
      this.errStackTrace = ex.error.stackTrace;
    }
  }

  compareToDev() {
    this.devBranch = _find(this.branchLeaderboard, o => o.branchName === 'develop');

    this.branchLeaderboard = _without(this.branchLeaderboard, this.devBranch);

    this.branchLeaderboard.map(b => {
      b.totalUnitTests -= this.devBranch.totalUnitTests;

      if (b.totalIntegTests !== null) {
        b.totalIntegTests -= this.devBranch.totalIntegTests;
      }

      if (b.totalXATTests !== null) {
        b.totalXATTests -= this.devBranch.totalXATTests;
      }

      b.score = b.totalUnitTests
        + (b.totalIntegTests || 0)
        + (b.totalXATTests || 0)
        - (b.commitsBehind || 0)
        - (b.commitsAhead || 0);
    });

    this.sort(this.headerCollection[1]);
  }

  sort(header: SortableHeader): void {
    this.branchLeaderboard = sortData(header, this.headerCollection, this.branchLeaderboard);
  }

}

