import { NgModule } from '@angular/core';
import { CommonModule as NgCommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { NgSpinKitModule } from 'ng-spin-kit'

import { BranchLeaderboardComponent } from './branch-leaderboard/branch-leaderboard.component';

@NgModule({
  declarations: [
    BranchLeaderboardComponent,
  ],
  imports: [
    NgCommonModule,
    RouterModule,
    NgSpinKitModule
  ],
  exports: [
    BranchLeaderboardComponent,
  ]
})
export class BranchLeaderboardModule { }
