import { Component, OnInit } from '@angular/core';
import { DataService } from '../../services/data.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Branch } from '../../models/Branch';
import { finalize } from 'rxjs/operators';
import _ from "lodash";

@Component({
  selector: 'app-branch-comparison',
  templateUrl: './branch-comparison.component.html',
  styleUrls: ['./branch-comparison.component.less']
})
export class BranchComparisonComponent implements OnInit {

  branches: Branch[];
  EMPTY_BRANCH: Branch;
  sourceId: number = null;
  targetId: number = null;
  sourceBranch: string = null;

  constructor(private dataService: DataService, private router: Router, private route: ActivatedRoute) { }

  ngOnInit() {
    this.EMPTY_BRANCH = {
      branchId: null,
      branchName: "Please select a branch",
      repoId: null,
      repoName: null
    };

    this.getBranches();
  }

  getBranches(): void {
    let source = this.dataService.getBranches();

    source.pipe(
      finalize(() => {}),
    ).subscribe(
      (branches: Branch[]) => {
        this.branches = branches;
        this.branches.unshift(this.EMPTY_BRANCH);

        this.setTargetBranch();
        this.selectDefaultBranches();
      },
      ex => {
        if (ex.status === 500) {
          //Internal Server Error
          console.error(ex.error.exceptionMessage, ex.error.stackTrace);
        }
      }
    );
  }

  setTargetBranch() {
    const devBranch = this.branches.find(b => b.branchName === 'develop');

    if (devBranch) {
      this.targetId = devBranch.branchId;
    }
  }

  updateBranches() {
    if (this.sourceId) {
      this.router.navigate(['branchComparison'], {
        queryParams: { sourceId: this.sourceId}
      });

      this.sourceBranch = this.branches.find(b => b.branchId === this.sourceId).branchName;
    }
  }

  selectDefaultBranches() {
    if (this.route.snapshot.queryParams['sourceId']) {
      this.sourceId = +this.route.snapshot.queryParams['sourceId']
      this.sourceBranch = this.branches.find(b => b.branchId === this.sourceId).branchName;
    }
  }
}
