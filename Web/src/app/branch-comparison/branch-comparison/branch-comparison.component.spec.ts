import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BranchComparisonComponent } from './branch-comparison.component';

describe('BranchComparisonComponent', () => {
  let component: BranchComparisonComponent;
  let fixture: ComponentFixture<BranchComparisonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BranchComparisonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BranchComparisonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
