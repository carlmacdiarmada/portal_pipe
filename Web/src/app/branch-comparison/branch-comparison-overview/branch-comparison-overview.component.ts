import { Component, OnChanges, Input, SimpleChanges } from '@angular/core';
import { DataService } from './../../services/data.service';
import { BranchDetails } from './../../models/BranchDetails';
import { WIDGET_STATE } from './../../models/Enums';

@Component({
  selector: 'branch-comparison-overview',
  templateUrl: './branch-comparison-overview.component.html',
  styleUrls: ['./branch-comparison-overview.component.less']
})
export class BranchComparisonOverviewComponent implements OnChanges {

  @Input() sourceId: number;
  @Input() targetId: number;

  sourceDetails: BranchDetails;
  targetDetails: BranchDetails;

  WIDGET_STATE: any = WIDGET_STATE;
  state: WIDGET_STATE = WIDGET_STATE.Unitialized;
  errMsg: string = "";
  errStackTrace: string = "";

  constructor(private dataService: DataService) { }

  ngOnChanges(changesObj: SimpleChanges) {
    if (this.sourceId && this.targetId) {
      this.getProjectDetails();
    }
    else {
      this.state = WIDGET_STATE.Unitialized;
    }
  }

  getProjectDetails() {
    this.state = WIDGET_STATE.Loading;

    this.dataService.getProjectDetails(this.sourceId)
      .subscribe((branchDetails: BranchDetails) => {
        this.sourceDetails = branchDetails;
      }, this.httpError);

    this.dataService.getProjectDetails(this.targetId)
      .subscribe((branchDetails: BranchDetails) => {
        this.targetDetails = branchDetails;

        this.state = WIDGET_STATE.Success;
      }, this.httpError);
  }

  httpError(ex) {
    this.state = WIDGET_STATE.Failure;

    if (ex.status === 500) {
      //Internal Server Error
      this.errMsg = ex.error.exceptionMessage;
      this.errStackTrace = ex.error.stackTrace;
    }
  }

}
