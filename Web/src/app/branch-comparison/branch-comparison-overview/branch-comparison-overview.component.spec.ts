import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BranchComparisonOverviewComponent } from './branch-comparison-overview.component';

describe('BranchComparisonOverviewComponent', () => {
  let component: BranchComparisonOverviewComponent;
  let fixture: ComponentFixture<BranchComparisonOverviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BranchComparisonOverviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BranchComparisonOverviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
