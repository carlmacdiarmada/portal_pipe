import { Component, OnChanges, Input, SimpleChanges } from '@angular/core';
import { find as _find } from "lodash";
import { CompareResult } from './../../models/CompareResult';
import { WIDGET_STATE } from './../../models/Enums';
import { DataService } from './../../services/data.service';
import { finalize } from 'rxjs/operators';
import { TestResult } from '../../models/TestResult';
import { ComparisonDetail } from '../../models/ComparisonDetail';
import { getTestResultObject } from '../../utils';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { SortableHeader } from '../../models/SortableHeader';
import { sortData } from './../../utils';

@Component({
  selector: 'branch-comparison-details',
  templateUrl: './branch-comparison-details.component.html',
  styleUrls: ['./branch-comparison-details.component.less']
})
export class BranchComparisonDetailsComponent implements OnChanges {

  @Input() sourceId: number;
  @Input() targetId: number;

  results: any = [];

  WIDGET_STATE: any = WIDGET_STATE;
  state: WIDGET_STATE = WIDGET_STATE.Unitialized;
  errMsg: string = "";
  errStackTrace: string = "";
  headerCollection: SortableHeader[];

  constructor(private dataService: DataService, private modalService: NgbModal) { }

  ngOnInit() {
    this.headerCollection = [
      <SortableHeader>({allowSort: true, displayName: 'Job Name', propName: 'jobName', direction: null}),
      <SortableHeader>({allowSort: true, displayName: 'Job Type', propName: 'jobType', direction: null}),
      <SortableHeader>({allowSort: true, displayName: 'Source', propName: 'startedAtSource', direction: null}),
      <SortableHeader>({allowSort: true, displayName: 'Destination', propName: 'startedAtTarget', direction: null}),
      <SortableHeader>({allowSort: false, displayName: 'Diff'}),
    ]
  }

  ngOnChanges(changesObj: SimpleChanges) {
    if (this.sourceId && this.targetId) {
      this.getBranchComparison();
    }
    else {
      this.state = WIDGET_STATE.Unitialized;
    }
  }

  getBranchComparison() {
    this.state = WIDGET_STATE.Loading;

    let source = this.dataService.compareBranches(this.sourceId, this.targetId);

    source.pipe(
      finalize(() => {}),
    ).subscribe(this.httpSuccess.bind(this), this.httpError);
  }

  httpSuccess(comparisonResults: CompareResult[]){
    this.state = WIDGET_STATE.Success;

    this.results = comparisonResults.map(this.getComparisonDetail);
  }

  httpError(ex) {
    this.state = WIDGET_STATE.Failure;

    if (ex.status === 500) {
      //Internal Server Error
      this.errMsg = ex.error.exceptionMessage;
      this.errStackTrace = ex.error.stackTrace;
    }
  }

  getComparisonDetail(item: CompareResult): ComparisonDetail{
      const jobName = item.sourceBranch.jobName || item.targetBranch.jobName;
      const jobType = item.sourceBranch.jobType || item.targetBranch.jobType;
      const startedAtSource = item.sourceBranch.startedAt;
      const startedAtTarget = item.targetBranch.startedAt;

      let sourceBranchResults: TestResult, targetBranchResults: TestResult;

      if (item.sourceBranch.jobName) {
        sourceBranchResults = getTestResultObject(
          item.sourceBranch.passed,
          item.sourceBranch.failed,
          item.sourceBranch.skipped,
          item.sourceBranch.linesCovered,
          item.sourceBranch.totalLines
        );
      }

      if (item.targetBranch.jobName) {
        targetBranchResults = getTestResultObject(
          item.targetBranch.passed,
          item.targetBranch.failed,
          item.targetBranch.skipped,
          item.targetBranch.linesCovered,
          item.targetBranch.totalLines
        );
      }

      return <ComparisonDetail>({
        jobName: jobName,
        jobType: jobType,
        startedAtSource: startedAtSource,
        startedAtTarget: startedAtTarget,
        sourceBranchResults: sourceBranchResults,
        targetBranchResults: targetBranchResults
      });
  }

  showHelp(modal: any) {
    this.modalService.open(modal);
  }

  sort(header: SortableHeader): void {
    this.results = sortData(header, this.headerCollection, this.results);
  }
}
