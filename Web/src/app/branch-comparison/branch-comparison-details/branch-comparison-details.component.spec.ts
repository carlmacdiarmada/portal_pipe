import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BranchComparisonDetailsComponent } from './branch-comparison-details.component';

describe('BranchComparisonDetailsComponent', () => {
  let component: BranchComparisonDetailsComponent;
  let fixture: ComponentFixture<BranchComparisonDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BranchComparisonDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BranchComparisonDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
