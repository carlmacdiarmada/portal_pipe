import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BranchComparisonSummaryComponent } from './branch-comparison-summary.component';

describe('BranchComparisonSummaryComponent', () => {
  let component: BranchComparisonSummaryComponent;
  let fixture: ComponentFixture<BranchComparisonSummaryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BranchComparisonSummaryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BranchComparisonSummaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
