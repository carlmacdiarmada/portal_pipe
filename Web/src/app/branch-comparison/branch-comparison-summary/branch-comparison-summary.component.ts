import { Component, OnChanges, Input, SimpleChanges } from '@angular/core';
import { WIDGET_STATE } from './../../models/Enums';
import { find as _find } from 'lodash';
import { CompareResult } from './../../models/CompareResult';
import { DataService } from './../../services/data.service';
import _ from "lodash";
import { finalize } from 'rxjs/operators';
import { getTestResultObject } from './../../utils';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'branch-comparison-summary',
  templateUrl: './branch-comparison-summary.component.html',
  styleUrls: ['./branch-comparison-summary.component.less']
})
export class BranchComparisonSummaryComponent implements OnChanges {

  @Input() sourceId: number;
  @Input() targetId: number;

  WIDGET_STATE: any = WIDGET_STATE;
  state = WIDGET_STATE.Unitialized;
  comparisonResults: CompareResult;

  sourceBranch: any = {};
  targetBranch: any = {};

  errMsg: string = "";
  errStackTrace: string = "";

  constructor(private dataService: DataService, private modalService: NgbModal) { }

  ngOnChanges(changesObj: SimpleChanges) {
    if (this.sourceId && this.targetId) {
      this.getBranchComparison();
    }
    else {
      this.state = WIDGET_STATE.Unitialized;
    }
  }

  getBranchComparison() {
    this.state = WIDGET_STATE.Loading;

    let source = this.dataService.compareBranches(this.sourceId, this.targetId);

    source.pipe(
      finalize(() => {}),
    ).subscribe(this.httpSuccess.bind(this), this.httpError);
  }

  httpSuccess(compareResult: CompareResult){
    this.comparisonResults = compareResult;

    const sourceSummary = this.getComparisonSummary("sourceBranch");
    const targetSummary = this.getComparisonSummary("targetBranch");

    this.state = WIDGET_STATE.Success;

    this.sourceBranch = {
      unit: this.findTestResultsObj(sourceSummary,"UnitTest"),
      integ: this.findTestResultsObj(sourceSummary,"IntegTest"),
      xat: this.findTestResultsObj(sourceSummary, "XATS")
    };

    this.targetBranch = {
      unit: this.findTestResultsObj(targetSummary,"UnitTest"),
      integ: this.findTestResultsObj(targetSummary,"IntegTest"),
      xat: this.findTestResultsObj(targetSummary,"XATS")
    };
  }

  httpError(ex) {
    this.state = WIDGET_STATE.Failure;

    if (ex.status === 500) {
      //Internal Server Error
      this.errMsg = ex.error.exceptionMessage;
      this.errStackTrace = ex.error.stackTrace;
    }
  }

  public getComparisonSummary(branchType) {
    const results = _
      .chain(this.comparisonResults)
      .map(o => ({
        type: o[branchType].jobType,
        passed: o[branchType].passed,
        failed: o[branchType].failed,
        skipped: o[branchType].skipped,
        linesCovered: o[branchType].linesCovered,
        totalLines: o[branchType].totalLines
      }))
      .groupBy(o => o.type)
      .filter(o => _.head(o).type !== "")
      .map(o => {
        const ch = _.chain(o);
        const totalPassed = ch.sumBy(o => o.passed).value();
        const totalFailed = ch.sumBy(o => o.failed).value();
        const totalSkipped = ch.sumBy(o => o.skipped).value();
        const linesCovered = ch.sumBy(o =>o.linesCovered).value();
        const totalLines = ch.sumBy(o =>o.totalLines).value();
        let output = {
          name: _.head(o).type || "IntegTest",
          testResults: getTestResultObject(
            totalPassed,
            totalFailed,
            totalSkipped,
            linesCovered,
            totalLines
          )
        };

        return output;
      })
      .value();

    return results;
  }

  findTestResultsObj(parentObj, type) {
    const obj = _find(parentObj, o => o.name === type);

    if (obj) {
      return obj.testResults;
    }
  }

  showHelp(modal: any) {
    this.modalService.open(modal);
  }

}
