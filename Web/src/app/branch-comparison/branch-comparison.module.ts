import { NgModule } from '@angular/core';
import { CommonModule as NgCommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { NgSpinKitModule } from 'ng-spin-kit'
import { CommonModule } from './../common/common.module';
import { FormsModule } from '@angular/forms';

import { BranchComparisonComponent } from './branch-comparison/branch-comparison.component';
import { BranchComparisonOverviewComponent } from './branch-comparison-overview/branch-comparison-overview.component';
import { BranchComparisonSummaryComponent } from './branch-comparison-summary/branch-comparison-summary.component';
import { BranchComparisonDetailsComponent } from './branch-comparison-details/branch-comparison-details.component';

@NgModule({
  declarations: [
    BranchComparisonComponent,
    BranchComparisonOverviewComponent,
    BranchComparisonSummaryComponent,
    BranchComparisonDetailsComponent,
  ],
  imports: [
    RouterModule,
    NgSpinKitModule,
    NgCommonModule,
    CommonModule,
    FormsModule
  ],
  exports: [
    BranchComparisonComponent,
  ]
})
export class BranchComparisonModule { }
