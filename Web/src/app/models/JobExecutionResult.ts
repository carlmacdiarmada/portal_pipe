export class JobExecutionResult {
    jobName: string;
    type: string;
    outcome: string;
    waitTime: number;
    blockedTime: number;
    duration: number;
    startedAt: Date;
}