export class BranchDetails {
    branchName: string;
    totalUnitTests: number;
    totalIntegTests: number;
    totalXATTests: number;
    lastUpdated: Date;
    commitsBehind: number;
    commitsAhead: number;
    score: number;
}