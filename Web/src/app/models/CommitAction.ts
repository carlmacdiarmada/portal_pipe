export class CommitAction {
    name: string;
    email: string;
    date: Date;
}