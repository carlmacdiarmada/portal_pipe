import { Commit } from './Commit';

export class BranchStats {
    name: string;
    aheadCount: number;
    behindCount: number;
    isBaseVersion: number;
    commit: Commit;
}