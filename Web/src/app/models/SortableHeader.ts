export class SortableHeader {
    allowSort: boolean;
    displayName: string;
    propName: string;
    direction: string = null;
    cssClass: string;
}