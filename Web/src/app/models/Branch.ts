export class Branch {
    branchId: number;
    branchName: string;
    repoId: number;
    repoName: string;
}