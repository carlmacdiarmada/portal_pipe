export enum WIDGET_STATE {
    Unitialized = 0,
    Loading = 1,
    Success = 2,
    Failure = 3
}

export enum DATE_RANGE_TYPE {
    Last30Days = 1,
    Last7Days = 2,
    Last24Hours = 3
}

export enum BUILD_TYPE {
    All = 1,
    Project = 2,
    PR = 3
}

export enum JOB_OUTCOME_TYPE {
    All = 1,
    Successful = 2,
    Failed = 3,
    Aborted = 4
}