import { CommitAction } from './CommitAction';

export class Commit {
    author: CommitAction;
    committer: CommitAction;
    comment: string;
    webLink: string;
}