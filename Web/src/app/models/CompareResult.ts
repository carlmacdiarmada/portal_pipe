import { CompareJobResult } from './CompareJobResult';

export class CompareResult {
    sourceBranch: CompareJobResult;
    targetBranch: CompareJobResult;
}