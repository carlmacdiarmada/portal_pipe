import { DATE_RANGE_TYPE, BUILD_TYPE, JOB_OUTCOME_TYPE } from './Enums';

export class BranchAnalyticsCriteria {
    dateRange: DATE_RANGE_TYPE;
    buildType: BUILD_TYPE;
    outcomeType: JOB_OUTCOME_TYPE;
    includeWeekends: boolean;
}