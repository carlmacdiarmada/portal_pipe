import { TestResult } from './TestResult';

export class ComparisonDetail {
    jobName: string;
    jobType: string;
    startedAtSource: Date;
    startedAtTarget: Date;
    sourceBranchResults: TestResult;
    targetBranchResults: TestResult;
}