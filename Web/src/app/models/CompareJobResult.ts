export class CompareJobResult {
    jobName: string;
    jobType: string;
    outcome: string;
    passed: number;
    failed: number;
    skipped: number;
    linesCovered: number;
    totalLines: number;
    startedAt: Date;
}