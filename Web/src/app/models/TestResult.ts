export class TestResult {
    totalPassed: number;
    totalFailed: number;
    totalSkipped: number;
    linesCovered: number;
    totalLines: number;
    total: number;
    passed: number;
    failed: number;
    skipped: number;
    coverage: number;
    startTime: Date;
}