export class JobStatus {
    shortName: string;
    jobName: string;
    jobUrl: string;
    lastRun: Date;
    outcome: string;
}