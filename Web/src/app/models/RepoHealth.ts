import { TestResult } from './TestResult';

export class RepoHealth {
    repoId: number;
    repo: string;
    branchId: number;
    branch: number;
    lastCommitId: number;
    lastShaId: string;
    testResults: TestResult;
}