import { TestResult } from './TestResult';

export class JobResult {
    jobName: string;
    jobURL: string;
    timeTaken: number;
    shaId: string;
    lastShaId: string;
    unitTestResults: TestResult;
    integTestResults: TestResult;
    xatTestResults: TestResult;
    commitTime: Date;
    lastCommitTime: Date;
}