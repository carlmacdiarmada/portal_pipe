import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { RepoHealth } from '../models/RepoHealth';
import { Branch } from '../models/Branch';
import { JobStatus } from '../models/JobStatus';
import { JobResult } from '../models/JobResult';
import { CommitTime } from '../models/CommitTime';
import { Commit } from '../models/Commit';
import { BranchDetails } from '../models/BranchDetails';
import { CompareResult } from '../models/CompareResult';
import { environment } from '../../environments/environment';
import { BranchStats } from '../models/BranchStats';
import { JobExecutionResult } from '../models/JobExecutionResult';
import { BranchAnalyticsCriteria } from '../models/BranchAnalyticsCriteria';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  private apiBaseUrl = environment.apiBaseUrl;  // URL to web api

  constructor(private http: HttpClient) { }

  getReposHealth (): Observable<RepoHealth[]> {
    return this.http
      .get<RepoHealth[]>(`${this.apiBaseUrl}/drilldown/reposhealth`);
  }

  getBranches (): Observable<Branch[]> {
    return this.http
      .get<Branch[]>(`${this.apiBaseUrl}/drilldown/branches`);
  }

  getBranchStatus (branchId: number): Observable<JobStatus[]> {
    return this.http
      .get<JobStatus[]>(`${this.apiBaseUrl}/drilldown/branchstatus/${branchId}`);
  }

  getBranchHealth(branchId: number): Observable<JobResult[]> {
    return this.http
      .get<JobResult[]>(`${this.apiBaseUrl}/drilldown/branchhealth/${branchId}`);
  }

  getCommitTimes(repo: string, shaId: string, lastShaId: string): Observable<CommitTime> {
    return this.http
      .get<CommitTime>(`${this.apiBaseUrl}/drilldown/committimes/${repo}/?shaId=${
        shaId ? shaId : ""
      }&lastShaId=${lastShaId ? lastShaId : ""}`
      );
  }

  getCommitDetails(repo: string, sha: string): Observable<Commit> {
    return this.http
      .get<Commit>(`${this.apiBaseUrl}/drilldown/commitdetails/${repo}/${sha}`);
  }

  getProjectDetails(branchId: number): Observable<BranchDetails> {
    return this.http
      .get<BranchDetails>(`${this.apiBaseUrl}/drilldown/branchesDetails/${branchId}`);
  }

  compareBranches(sourceId: number, targetId: number): Observable<CompareResult> {
    return this.http
      .get<CompareResult>(`${this.apiBaseUrl}/drilldown/compareBranches/${sourceId}/${targetId}`);
  }

  getBranchStats(sourceBranch: string): Observable<BranchStats> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json'
      })
    };

    return this.http
      .post<BranchStats>(`${this.apiBaseUrl}/drilldown/branchstats/`, "\"" + sourceBranch + "\"", httpOptions);
  }

  getBranchLeaderboard(): Observable<BranchDetails[]> {
    return this.http
      .get<BranchDetails[]>(`${this.apiBaseUrl}/drilldown/branchesLeaderboard/`);
  }

  getBranchAnalytics(criteria: BranchAnalyticsCriteria): Observable<JobExecutionResult[]> {
    return this.http
      .post<JobExecutionResult[]>(`${this.apiBaseUrl}/drilldown/branchanalytics/`, criteria);
  }
}
