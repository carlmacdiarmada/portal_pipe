﻿using Sdm.Testing.QuarantineDashboard.DataAccess.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace Sdm.Testing.QuarantineDashboard.Api.Controllers
{
    [RoutePrefix("api/CIEnvironment")]
    public class CIEnvironmentController : ApiController
    {
        [Route("AllocateEnvironment"), HttpPost]
        public async Task<IHttpActionResult> PostCIEnvironment([FromUri]string project)
        {
            var repo = new CIEnvironmentRepository();
            List<string> results = await repo.PostCIEnvironment(project);
            string message = results.First();
            if (message.Contains("There are no available databases left for allocation"))
            {
                return new System.Web.Http.Results.ResponseMessageResult(
                    Request.CreateResponse(
                        (HttpStatusCode)423,
                        new HttpError(message)
                    )
                );
            }
            else if (message.Contains("Project already allocated"))
            {
                string env = message.Split(' ').Last();
                return  Ok(project + " is already allocated to " + env);
            }
            else
            {
                return new System.Web.Http.Results.ResponseMessageResult(
                    Request.CreateResponse(
                        HttpStatusCode.Created,
                        (project + " has been allocated to " + message)
                    )
                );
            }
        }
        [Route("DeallocateEnvironment"), HttpDelete]
        public async Task<IHttpActionResult> DeleteCIEnvironment([FromUri]string project)
        {
            var repo = new CIEnvironmentRepository();
            List<string> response = await repo.DeleteCIEnvironment(project);
            string message = response.First();
            if (message.Contains("Project not found"))
            {
                return new System.Web.Http.Results.ResponseMessageResult(
                    Request.CreateResponse(
                        HttpStatusCode.NotFound,
                        message
                    )
                );
            }
            else
            {
                return new System.Web.Http.Results.ResponseMessageResult(
                    Request.CreateResponse(
                        HttpStatusCode.OK,
                        (project + " has been deallocated")
                    )
                );
            }
        }
    }
}