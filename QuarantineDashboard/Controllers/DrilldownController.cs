﻿using Sdm.Testing.QuarantineDashboard.DataAccess;
using Sdm.Testing.QuarantineDashboard.DataAccess.Models;
using Sdm.Testing.QuarantineDashboard.DataAccess.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace Sdm.Testing.QuarantineDashboard.Api.Controllers
{
    [RoutePrefix("api/drilldown")]
    public class DrilldownController : ApiController
    {
        [Route("reposhealth"), HttpGet]
        public async Task<IHttpActionResult> GetReposHealthAsync()
        {
            var repo = new DrilldownRepository();
            return Ok(await repo.GetReposHealthAsync());
        }

        [Route("branchhealth/{branchId}"), HttpGet]
        public async Task<IHttpActionResult> GetBranchHealthAsync(int branchId)
        {
            var repo = new DrilldownRepository();
            return Ok(await repo.GetBranchHealthAsync(branchId));
        }

        [Route("branches"), HttpGet]
        public async Task<IHttpActionResult> GetBranches()
        {
            var repo = new DrilldownRepository();
            return Ok(await repo.GetBranchesAsync());
        }

        [Route("branchesDetails/{branchId}"), HttpGet]
        public async Task<IHttpActionResult> GetBranchDetails(int branchId)
        {
            var repo = new DrilldownRepository();
            return Ok(await repo.GetBranchDetailsAsync(branchId));
        }

        [Route("branchstatus/{branchId}"), HttpGet]
        public async Task<IHttpActionResult> GetBranchStatusAsync(int branchId)
        {
            var repo = new DrilldownRepository();
            return Ok(await repo.GetBranchStatusAsync(branchId));
        }

        [Route("comparebranches/{sourceId}/{targetId}"), HttpGet]
        public async Task<IHttpActionResult> CompareBranchesAsync(int sourceId, int targetId)
        {
            var repo = new DrilldownRepository();
            return Ok(await repo.CompareBranchesAsync(sourceId, targetId));
        }

        [Route("commitdetails/{repoName}/{sha}"), HttpGet]
        public async Task<IHttpActionResult> GetCommitDetailsAsync(string repoName, string sha)
        {
            var repo = new DrilldownRepository();
            return Ok(await repo.GetCommitDetailsAsync(repoName, sha));
        }

        [Route("committimes/{repoName}"), HttpGet]
        public async Task<IHttpActionResult> GetCommitTimesAsync(string repoName, string shaId = null, string lastShaId = null)
        {
            var repo = new DrilldownRepository();
            return Ok(await repo.GetCommitTimesAsync(repoName, shaId, lastShaId));
        }

        [Route("branchstats"), HttpPost]
        public async Task<IHttpActionResult> GetBranchStatsAsync([FromBody]string sourceBranch)
        {
            var repo = new DrilldownRepository();
            return Ok(await repo.GetBranchStatsAsync(sourceBranch));
        }

        [Route("branchesLeaderboard"), HttpGet]
        public async Task<IHttpActionResult> GetBranchLeaderboardAsync()
        {
            var repo = new DrilldownRepository();
            return Ok(await repo.GetBranchLeaderboardAsync());
        }

        [Route("branchAnalytics"), HttpPost]
        public async Task<IHttpActionResult> GetBranchAnalyticsAsync([FromBody]BranchAnalyticsCriteria criteria)
        {
            var repo = new DrilldownRepository();
            return Ok(await repo.GetBranchAnalyticsAsync(criteria));
        }
    }

}
