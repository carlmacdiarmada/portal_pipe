﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sdm.Testing.QuarantineDashboard.DataAccess
{
    public enum DateRangeType
    {
        Unknown = 0,
        Last30Days = 1,
        Last7Days = 2,
        Last24Hour = 3,
    }

    public enum BuildType
    {
        Unknown = 0,
        All = 1,
        Project = 2,
        PR = 3
    }

    public enum JobOutcomeType
    {
        Unknown = 0,
        All = 1,
        Successful = 2,
        Failed = 3,
        Aborted = 4,
    }
}
