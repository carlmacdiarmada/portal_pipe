﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Sdm.Testing.QuarantineDashboard.DataAccess.Models;
using Sdm.Testing.QuarantineDashboard.DataAccess.Utils;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace Sdm.Testing.QuarantineDashboard.DataAccess.Repositories
{

    public class DrilldownRepository
    {
        #region Properties

        private string TfsApiUrl
        {
            get { return "http://vssdmlivetfs:8080/tfs/BOMiLiveTFS/{0}/_apis/git/repositories/Trunk/"; }
        }

        private string TfsWebUrl
        {
            get { return "http://vssdmlivetfs:8080/tfs/BOMiLiveTFS/{0}/_git/Trunk/commit/{1}"; }
        }
        
        #endregion

        #region Data Access

        public async Task<List<RepoHealth>> GetReposHealthAsync()
        {
            var branches = GetActiveBranches();
            var results = new List<RepoHealth>();

            var sql = "usp_ro_Get_Repos_Health";

            using (var reader = await SqlExecutor.ExecuteReaderSPAsync(sql, null))
            {
                while (reader.Read())
                {
                    var repo = reader.GetString(1).Replace("/_git/Trunk", "");
                    var branch = reader.GetString(3).Replace("origin/", "");
                    
                    if (repo == "INTEG_MIRROR" && branches.Contains(branch))
                    {
                        results.Add(new RepoHealth()
                        {
                            RepoId = reader.GetInt64(0),
                            Repo = repo,
                            BranchId = reader.GetInt64(2),
                            Branch = branch,
                            LastCommitId = reader.GetInt64(4),
                            LastShaId = reader.GetString(5),
                            TestResults = new TestResult()
                            {
                                TotalPassed = reader.SafeGetInt32(6),
                                TotalFailed = reader.SafeGetInt32(7),
                                TotalSkipped = reader.SafeGetInt32(8)
                            },
                        });
                    }
                }
            }

            return results;
        }

        public async Task<List<JobResult>> GetBranchHealthAsync(long branchId)
        {
            var results = new List<JobResult>();

            var sql = "getLatestJobExecutionResults";

            var cmdParameters = new List<SqlParameter>()
            {
                new SqlParameter("@branchId", branchId)
            };

            using (var reader = await SqlExecutor.ExecuteReaderSPAsync(sql, cmdParameters))
            {
                while (reader.Read())
                {
                    UpsertJobResult(results, reader);
                }
            }

            results = results
                .Where(jr => jr.UnitTestResults != null || jr.IntegTestResults != null || jr.XatTestResults != null)
                .ToList();

            results.Insert(0, SumJobResults(results));

            return results;
        }

        public async Task<List<Branch>> GetBranchesAsync()
        {
            var branches = GetActiveBranches();
            var results = new List<Branch>();

            var sql = "usp_ro_Get_Branches";
            
            using (var reader = await SqlExecutor.ExecuteReaderSPAsync(sql, null))
            {
                while (reader.Read())
                {
                    var repo = reader.GetString(3).Replace("/_git/Trunk", "");
                    var branch = reader.GetString(1).Replace("origin/", "");

                    if (repo == "INTEG_MIRROR" && branches.Contains(branch))
                    {
                        results.Add(new Branch()
                        {
                            BranchId = reader.GetInt64(0),
                            BranchName = branch,
                            RepoId = reader.GetInt64(2),
                            RepoName = repo,
                        });
                    }
                }
            }           

            return results;
        }

        public async Task<List<BranchDetails>> GetBranchDetailsAsync(long branchId)
        {
            var results = new List<BranchDetails>();

            var sql = "getBranchDetails";

            var cmdParameters = new List<SqlParameter>()
            {
                new SqlParameter("@BranchId", branchId)
            };

            using (var reader = await SqlExecutor.ExecuteReaderSPAsync(sql, cmdParameters))
            {
                while (reader.Read())
                {
                    results.Add(new BranchDetails()
                    {
                        BranchName = reader["branchName"].ToString().Replace("origin/", ""),
                        TotalUnitTests = reader.SafeGetInt32("TotalUnit"),
                        TotalIntegTests = reader.SafeGetInt32("TotalInteg"),
                        TotalXATTests = reader.SafeGetInt32("TotalXAT"),
                        LastUpdated = reader.SafeGetDateTime("branchVersion")
                    });
                }
            }

            return results;
        }

        public async Task<List<BranchDetails>> GetBranchLeaderboardAsync()
        {
            var results = new List<BranchDetails>();

            var branches = await GetBranchesAsync();

            foreach(var b in branches)
            {
                if (b.BranchName.StartsWith("master") 
                    || b.BranchName.StartsWith("release")
                    || b.BranchName.StartsWith("livefix")
                    || b.BranchName.StartsWith("feature")) { continue; }

                var details = await GetBranchDetailsAsync(b.BranchId);

                var stats = await GetBranchStatsAsync(b.BranchName);

                details.First().CommitsAhead = stats.AheadCount;
                details.First().CommitsBehind = stats.BehindCount;

                results.Add(details.First());
            }

            return results;
        }

        public async Task<List<JobStatus>> GetBranchStatusAsync(long branchId)
        {
            var results = new List<JobStatus>();

            var sql = "usp_ro_Get_Branch_Status";

            var cmdParameters = new List<SqlParameter>()
            {
                new SqlParameter("@BranchId", branchId)
            };

            using (var reader = await SqlExecutor.ExecuteReaderSPAsync(sql, cmdParameters))
            {
                while (reader.Read())
                {
                    results.Add(new JobStatus()
                    {
                        ShortName = reader["testJobName"].ToString(),
                        JobName = reader["testJobName"].ToString(),
                        JobURL = reader["JobURL"]?.ToString() ?? "",
                        LastRun = reader.SafeGetDateTime("startedAt"),
                        Outcome = reader["Outcome"].ToString()
                    });
                }
            }

            return results;
        }

        public async Task<List<CompareResult>> CompareBranchesAsync(long sourceId, long targetId)
        {
            var results = new List<CompareResult>();

            var sql = "getMergeDetails";

            var cmdParameters = new List<SqlParameter>()
            {
                new SqlParameter("@mergeBranchId", sourceId),
                new SqlParameter("@mainBranchId", targetId)
                
            };

            using (var reader = await SqlExecutor.ExecuteReaderSPAsync(sql, cmdParameters))
            {
                while (reader.Read())
                {
                    results.Add(new CompareResult()
                    {
                        SourceBranch = new CompareJobResult()
                        {
                            JobName = reader["mergeBranchName"].ToString(),
                            JobType = reader["mergeBranchType"].ToString(),
                            Outcome = reader["mergeBranchOutcome"].ToString(),
                            Passed = reader.SafeGetInt32("passedMergeBranch"),
                            Failed = reader.SafeGetInt32("failedMergeBranch"),
                            Skipped = reader.SafeGetInt32("skippedMergeBranch"),
                            LinesCovered = reader.SafeGetInt32("LinesCoveredMerge"),
                            TotalLines = reader.SafeGetInt32("TotalLinesMerge"),
                            StartedAt = reader.SafeGetDateTime("startedAtMergeBranch"),
                        },
                        TargetBranch = new CompareJobResult()
                        {
                            JobName = reader["mainBranchName"].ToString(),
                            JobType = reader["mainBranchType"].ToString(),
                            Outcome = reader["mainBranchOutcome"].ToString(),
                            Passed = reader.SafeGetInt32("passedMainBranch"),
                            Failed = reader.SafeGetInt32("failedMainBranch"),
                            Skipped = reader.SafeGetInt32("skippedMainBranch"),
                            LinesCovered = reader.SafeGetInt32("LinesCoveredMain"),
                            TotalLines = reader.SafeGetInt32("TotalLinesMain"),
                            StartedAt = reader.SafeGetDateTime("startedAtMainBranch")
                        },
                    });
                }
            }

            return results;
        }

        public async Task<Commit> GetCommitDetailsAsync(string repo, string sha)
        {
            return GetCommitDetails(repo, sha);
        }

        public async Task<Tuple<DateTime, DateTime>> GetCommitTimesAsync(string repo, string shaId = null, string lastShaId = null)
        {
            if (string.IsNullOrEmpty(shaId) || string.IsNullOrEmpty(lastShaId))
            {
                return null;
            }

            var commit = GetCommitDetails(repo, shaId);
            var lastCommit = GetCommitDetails(repo, lastShaId);

            return new Tuple<DateTime, DateTime>(commit.Committer.Date, lastCommit.Committer.Date);
        }

        public async Task<BranchStats> GetBranchStatsAsync(string sourceBranch)
        {
            using (var handler = new HttpClientHandler { Credentials = new NetworkCredential("sdmbuild1", "building1") })
            {
                using (var client = new HttpClient(handler))
                {
                    var url = string.Format(TfsApiUrl + "stats/branches?name={1}", "INTEG_MIRROR", sourceBranch);

                    var response = client.GetAsync(url).Result;
                    var json = new StreamReader(response.Content.ReadAsStreamAsync().Result, Encoding.UTF8).ReadToEnd();
                    var stats = JsonConvert.DeserializeObject<BranchStats>(json);

                    return stats;
                }
            }
        }

        public async Task<List<JobExecutionResult>> GetBranchAnalyticsAsync(BranchAnalyticsCriteria criteria)
        {
            var results = new List<JobExecutionResult>();

            var sql = "usp_ro_Get_Branch_Analytics";
            
            var cmdParameters = new List<SqlParameter>()
            {
                new SqlParameter("@StartDate", criteria.StartDate),
                new SqlParameter("@EndDate", criteria.EndDate),
                new SqlParameter("@BuildType", (int)criteria.BuildType),
                new SqlParameter("@OutcomeType", (int)criteria.OutcomeType),
                new SqlParameter("@IncludeWeekends", criteria.IncludeWeekends),                
            };

            using (var reader = await SqlExecutor.ExecuteReaderSPAsync(sql, cmdParameters))
            {
                while (reader.Read())
                {
                    results.Add(new JobExecutionResult()
                    {
                        JobName = reader.GetString(0),
                        Type = reader.GetString(1),
                        Outcome = reader.GetString(3),
                        WaitTime = reader.GetInt32(2),
                        BlockedTime = reader.GetInt32(4),
                        Duration = reader.GetInt32(5),
                        StartedAt = reader.GetDateTime(6)
                    });
                }
            }

            return results;
        }

        #endregion

        #region Private Helpers

        private void UpsertJobResult(List<JobResult> jobResults, SqlDataReader reader)
        {
            var jobName = reader["testJobName"].ToString();

            JobResult job;

            if (jobResults.FirstOrDefault(jr => jr.JobName == jobName) != null)
            {
                //Job Already exist
                job = jobResults.FirstOrDefault(jr => jr.JobName == jobName);
                job.JobURL = job.JobURL == "" ? (reader["JobURL"]?.ToString() ?? "") : job.JobURL;
            }
            else
            {
                //Create a new Test Job     
                job = new JobResult();
                job.JobName = jobName;
                job.JobURL = reader["JobURL"]?.ToString() ?? "";
                job.TimeTaken = reader.SafeGetInt32("TimeTakenMs").HasValue ? reader.SafeGetInt32("TimeTakenMs").Value : 0;
                job.ShaId = reader["ShaId"].ToString();
                job.LastShaId = reader["LastShaId"].ToString();

                jobResults.Add(job);
            }

            AddJobResult(job, reader);
        }   
        
        private void AddJobResult(JobResult job, SqlDataReader reader)
        {
            var testResult = new TestResult()
            {
                TotalPassed = reader.SafeGetInt32("numberSucceeded").Value,
                TotalFailed = reader.SafeGetInt32("numberFailed").Value,
                TotalSkipped = reader.SafeGetInt32("numberNotExecuted").Value,
                StartTime = reader.SafeGetDateTime("startedAt"),
                LinesCovered = reader.SafeGetInt32("LinesCovered"),
                TotalLines = reader.SafeGetInt32("TotalLines"),
            };

            switch (reader["Type"].ToString().ToLower())
            {
                case "unittest":
                    job.UnitTestResults = testResult;
                    break;

                case "integtest":
                    job.IntegTestResults = testResult;
                    break;

                case "xats":
                    job.XatTestResults = testResult;
                    break;
            }
        }
        
        private JobResult SumJobResults(List<JobResult> jobResults)
        {
            return new JobResult()
            {
                JobName = "All",
                UnitTestResults = SumTestResult(jobResults, jr => jr.UnitTestResults),
                IntegTestResults = SumTestResult(jobResults, jr => jr.IntegTestResults),
                XatTestResults = SumTestResult(jobResults, jr => jr.XatTestResults)
            };
        }

        private TestResult SumTestResult(List<JobResult> jobResults, Func<JobResult, TestResult> selector)
        {
            var testResults = jobResults
                .Select(selector)
                .Where(res => res != null);

            return new TestResult()
            {
                TotalPassed = (int)testResults.Sum(tr => tr.TotalPassed),
                TotalFailed = (int)testResults.Sum(tr => tr.TotalFailed),
                TotalSkipped = (int)testResults.Sum(tr => tr.TotalSkipped),
            };
        }

        private Commit GetCommitDetails(string repo, string sha)
        {
            using (var handler = new HttpClientHandler { Credentials = new NetworkCredential("sdmbuild1", "building1") })
            {
                using (var client = new HttpClient(handler))
                {
                    var url = string.Format(TfsApiUrl + "commits/{1}", repo, sha);
                                        
                    var response = client.GetAsync(url).Result;
                    var json = new StreamReader(response.Content.ReadAsStreamAsync().Result, Encoding.UTF8).ReadToEnd();
                    var commit = JsonConvert.DeserializeObject<Commit>(json);

                    commit.WebLink = string.Format(TfsWebUrl, repo, sha);

                    return commit;
                }
            }
        }

        private List<string> GetActiveBranches()
        {
            using (var handler = new HttpClientHandler { Credentials = new NetworkCredential("sdmbuild1", "building1") })
            {
                using (var client = new HttpClient(handler))
                {
                    var url = string.Format(TfsApiUrl + "stats/branches", "INTEG_MIRROR");


                    var response = client.GetAsync(url).Result;
                    var json = new StreamReader(response.Content.ReadAsStreamAsync().Result, Encoding.UTF8).ReadToEnd();
                    var branches = JObject.Parse(json)["value"] as JArray;

                    return branches.Select(b => b.Value<string>("name")).ToList();
                }
            }
        }

        #endregion
    }
}
