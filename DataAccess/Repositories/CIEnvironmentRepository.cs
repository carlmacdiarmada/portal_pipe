﻿using Sdm.Testing.QuarantineDashboard.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sdm.Testing.QuarantineDashboard.DataAccess.Repositories
{
    public class CIEnvironmentRepository
    {
        public async Task<List<string>> PostCIEnvironment(string project)
        {
            var results = new List<string>();

            var sql = "usp_rw_Lock_Environment";
            var cmdParameters = new List<SqlParameter>()
            {
                new SqlParameter("@project", project)
            };

            using (var reader = await SqlExecutor.ExecuteReaderSPAsync(sql, cmdParameters))
            {
                while (reader.Read())
                {
                    string message = reader.GetString(0);
                    results.Add(message);
                }
            }

            return results;
        }

        public async Task<List<string>> DeleteCIEnvironment(string project)
        {
            List<string> results = new List<string>();
            var sql = "usp_rw_Unlock_Environment";
            var cmdParameters = new List<SqlParameter>()
            {
                new SqlParameter("@project", project)
            };

            using (var reader = await SqlExecutor.ExecuteReaderSPAsync(sql, cmdParameters))
            {
                while (reader.Read())
                {
                    string message = reader.GetString(0);
                    results.Add(message);
                }
            }
            return results;
        }
    }
}
