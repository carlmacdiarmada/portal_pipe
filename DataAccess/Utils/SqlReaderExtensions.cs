﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sdm.Testing.QuarantineDashboard.DataAccess.Utils
{
    public static class SqlReaderExtensions
    {
        public static int? SafeGetInt32(this SqlDataReader reader, string fieldName)
        {
            var value = reader[fieldName].GetType() == typeof(System.DBNull)
                ? null
                : Convert.ToInt32(reader[fieldName]) as int?;

            return value;
        }

        public static int SafeGetInt32(this SqlDataReader reader, int index)
        {
            var value = reader[index].GetType() == typeof(System.DBNull)
                ? 0
                : Convert.ToInt32(reader[index]);

            return value;
        }

        public static DateTime? SafeGetDateTime(this SqlDataReader reader, string fieldName)
        {
            var value = reader[fieldName].GetType() == typeof(System.DBNull)
                ? null
                : Convert.ToDateTime(reader[fieldName]) as DateTime?;

            return value;
        }
    }
}
