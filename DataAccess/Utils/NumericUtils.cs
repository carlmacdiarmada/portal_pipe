﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sdm.Testing.QuarantineDashboard.DataAccess.Utils
{
    public static class NumericUtils
    {
        public static double Perentage(int value, int total)
        {
            return Math.Round((double)value / total * 100, 2);
        }

        public static double? Perentage(int? value, int? total)
        {
            if (!value.HasValue || !total.HasValue)
            {
                return null;
            }

            return Math.Round((double)value.Value / total.Value * 100, 2);
        }
    }
}
