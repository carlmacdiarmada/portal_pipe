﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sdm.Testing.QuarantineDashboard.DataAccess
{
    public static class SqlExecutor
    {
        #region Properties

        private static string ConnectionString
        {
            get
            {
                return ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            }
        }

        #endregion

        #region Public Methods                

        public static async Task<SqlDataReader> ExecuteReaderAsync(string sql)
        {
            try
            {
                var conn = new SqlConnection(ConnectionString);                
                conn.Open();

                var cmd = conn.CreateCommand();
                cmd.CommandTimeout = 600;
                cmd.CommandText = sql;
                      
                return await cmd.ExecuteReaderAsync(CommandBehavior.CloseConnection);
            }
            catch
            {
                throw;
            }
        }

        public static async Task<SqlDataReader> ExecuteReaderSPAsync(string sql, List<SqlParameter> cmdParameters)
        {
            var conn = new SqlConnection(ConnectionString);
            conn.Open();

            var cmd = conn.CreateCommand();
            cmd.CommandTimeout = 600;
            cmd.CommandText = sql;
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmdParameters != null)
            {
                cmd.Parameters.AddRange(cmdParameters.ToArray());
            }

            return await cmd.ExecuteReaderAsync(CommandBehavior.CloseConnection);            
        }

        #endregion
    }
}
