﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sdm.Testing.QuarantineDashboard.DataAccess.Models
{
    public class BranchAnalyticsCriteria
    {
        public DateRangeType DateRange { get; set; }

        public BuildType BuildType { get; set; }

        public JobOutcomeType OutcomeType { get; set; }

        public bool IncludeWeekends { get; set; }

        public DateTime StartDate { get { return GetStartDate(); } }

        public DateTime EndDate { get { return DateTime.Now; } }

        private DateTime GetStartDate()
        {
            switch (DateRange)
            {
                case DateRangeType.Last30Days:
                    return EndDate.AddDays(-30);

                case DateRangeType.Last7Days:
                    return EndDate.AddDays(-7);

                case DateRangeType.Last24Hour:
                    return EndDate.AddHours(-24);

                default:
                    throw new ArgumentException();
            }
        }
    }
}
