﻿using Sdm.Testing.QuarantineDashboard.DataAccess.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sdm.Testing.QuarantineDashboard.DataAccess.Models
{
    public class TestResult
    {
        public int TotalPassed { get; set; }

        public int TotalFailed { get; set; }

        public int TotalSkipped { get; set; }

        public int? LinesCovered { get; set; }

        public int? TotalLines { get; set; }

        public int Total { get { return TotalPassed + TotalFailed + TotalSkipped; } }

        public double Passed { get { return NumericUtils.Perentage(TotalPassed, Total); } }

        public double Failed { get { return NumericUtils.Perentage(TotalFailed, Total); } }

        public double Skipped { get { return NumericUtils.Perentage(TotalSkipped, Total); } }

        public double? Coverage { get { return NumericUtils.Perentage(LinesCovered, TotalLines); } }

        public DateTime? StartTime { get; set; }
    }
}
