﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sdm.Testing.QuarantineDashboard.DataAccess.Models
{
    public class CIEnvironment
    {
        public string Environment { get; set; }
        public string Project { get; set; }
    }
}
