﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sdm.Testing.QuarantineDashboard.DataAccess.Models
{
    public class Branch
    {
        public long BranchId { get; set; }

        public string BranchName { get; set; }

        public long RepoId { get; set; }

        public string RepoName { get; set; }
    }
}
