﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sdm.Testing.QuarantineDashboard.DataAccess.Models
{
    public class CommitAction
    {
        public string Name { get; set; }

        public string Email { get; set; }

        public DateTime Date { get; set; }
    }

    public class Commit
    {
        public CommitAction Author { get; set; }

        public CommitAction Committer { get; set; }

        public string Comment { get; set; }

        public string WebLink { get; set; }
    }
}
