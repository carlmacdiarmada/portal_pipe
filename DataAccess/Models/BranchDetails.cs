﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sdm.Testing.QuarantineDashboard.DataAccess.Models
{
    public class BranchDetails
    {
        public string BranchName { get; set; }

        public int? TotalUnitTests { get; set; }

        public int? TotalIntegTests { get; set; }

        public int? TotalXATTests { get; set; }

        public DateTime? LastUpdated { get; set; }

        public int CommitsAhead { get; set; }

        public int CommitsBehind { get; set; }      
        
        //Will be populated on the client side
        public int Score { get; set; }

        public override string ToString()
        {
            return $"{BranchName},{CommitsBehind},{CommitsAhead},{TotalUnitTests},{TotalIntegTests},{TotalXATTests}";
        }
    }
}
