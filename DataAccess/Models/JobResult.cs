﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sdm.Testing.QuarantineDashboard.DataAccess.Models
{
    public class JobResult
    {
        public string JobName { get; set; }

        public string JobURL { get; set; }

        public int TimeTaken { get; set; }

        public string ShaId { get; set; }

        public string LastShaId { get; set; }

        public TestResult UnitTestResults { get; set; }

        public TestResult IntegTestResults { get; set; }

        public TestResult XatTestResults { get; set; }
    }
}
