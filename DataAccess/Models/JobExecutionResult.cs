﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sdm.Testing.QuarantineDashboard.DataAccess.Models
{
    public class JobExecutionResult
    {
        public string JobName { get; set; }

        public string Type { get; set; }

        public string Outcome { get; set; }

        public int WaitTime { get; set; }

        public int BlockedTime { get; set; }

        public int Duration { get; set; }

        public DateTime StartedAt { get; set; }
    }
}
