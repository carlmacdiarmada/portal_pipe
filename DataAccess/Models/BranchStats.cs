﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sdm.Testing.QuarantineDashboard.DataAccess.Models
{
    public class BranchStats
    {
        public string Name { get; set; }

        public int AheadCount { get; set; }

        public int BehindCount { get; set; }

        public bool IsBaseVersion { get; set; }

        public Commit Commit { get; set; }
    }
}
