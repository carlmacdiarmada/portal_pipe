﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sdm.Testing.QuarantineDashboard.DataAccess.Models
{
    public class JobStatus
    {
        public string ShortName { get; set; }

        public string JobName { get; set; }
    
        public string JobURL { get; set; }

        public DateTime? LastRun { get; set; }

        public string Outcome { get; set; }
    }
}
