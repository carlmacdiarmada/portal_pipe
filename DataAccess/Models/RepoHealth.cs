﻿using Sdm.Testing.QuarantineDashboard.DataAccess.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sdm.Testing.QuarantineDashboard.DataAccess.Models
{
    public class RepoHealth
    {
        public long RepoId { get; set; }

        public string Repo { get; set; }

        public long BranchId { get; set; }

        public string Branch { get; set; }

        public long LastCommitId { get; set; }

        public string LastShaId { get; set; }

        public TestResult TestResults { get; set; }        
    }
}
