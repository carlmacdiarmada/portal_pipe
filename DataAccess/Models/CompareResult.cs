﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sdm.Testing.QuarantineDashboard.DataAccess.Models
{
    public class CompareJobResult
    {
        public string JobName { get; set; }

        public string JobType { get; set; }

        public string Outcome { get; set; }

        public int? Passed { get; set; }

        public int? Failed { get; set; }

        public int? Skipped { get; set; }

        public int? LinesCovered { get; set;}

        public int? TotalLines { get; set; }

        public DateTime? StartedAt { get; set; }
    }

    public class CompareResult
    {
        public CompareJobResult SourceBranch { get; set; }

        public CompareJobResult TargetBranch { get; set; }
    }
}
